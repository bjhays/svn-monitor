#!/usr/bin/python
#
# Script to notify the user for changes in a subversion repository.
#
# Depends on a configuration file with the following entries:
#
#   [server]
#   server=SVN_REPO_TO_MONITOR
#   user=YOUR_SVN_USERNAME
#   pass=YOUR_SVN_PASSWORD
#

try:
    import pysvn, pynotify, sys, bcrypt, os, getpass
    from simplecrypt import encrypt, decrypt
except:
    print "Error while loading external depencencies."
    print "Make sure the following packages are installed: 'pysvn','pynotify', 'bcrypt', 'simple-crypt'"
    exit()

import datetime, time, ConfigParser as cfg
chmod_def=0400


def read_config():
    """Read the configuration file containing server, username and password"""
    global svn_root, svn_username, svn_password, chmod_def

    args = sys.argv 
    try:
        config_file  = args[1]
    except:
        print "A config file is required"
        exit()

    #get salt or generate
    cipher_file=".cipher"
    if not os.path.isfile(cipher_file):
        print "Creating cipher_key"
        cipher_key=bcrypt.gensalt(20)
        f = open(cipher_file,'w')
        f.write('[cipher]\nkey='+cipher_key)
        f.close()
        os.chmod(cipher_file, chmod_def)
    else:
        parser = cfg.ConfigParser()
        parser.read(cipher_file)
        cipher_key = parser.get('cipher', 'key')

    if not os.path.isfile(config_file):
        svn_root = raw_input("Enter svn root url: ")
        svn_username = raw_input("Enter svn username:")
        svn_password = getpass.getpass("Enter svn password: ")
        svn_password_en = encrypt(cipher_key, svn_password)
        f = open(config_file,'w')
        f.write('[server]\nserver='+svn_root+'\nuser='+svn_username+'\npass='+svn_password_en)
        f.close()
        os.chmod(config_file, chmod_def)  

    config_header = 'server'

    try:
        parser = cfg.ConfigParser()
        parser.read(config_file)
        svn_root = parser.get(config_header, 'server')
        svn_username = parser.get(config_header, 'user')
        svn_password_en = parser.get(config_header, 'pass')
        svn_password = decrypt(cipher_key,svn_password_en)
    except BaseException as e:
        print "Error while parsing file '%s':" % config_file
        print e
        exit()

def notify(paths, authors):
    """Display the changed paths using libnotify"""
    title_string = 'New commits to repository'
    path_string = ', '.join(paths)
    author_string = ', '.join(authors)
    message_string = path_string
    
    if pynotify.init("SVN Monitor"):
        n = pynotify.Notification(title_string, message_string, "emblem-shared")
        n.show()

def log_message(paths, authors):
    """Print a log message containing the time, authors and paths"""
    now = datetime.datetime.now()
    now = now.strftime("%H:%M")
    path_string = ', '.join(paths)
    author_string = ', '.join(authors)
    print "[%s] Paths: %s -- Authors: %s" % (now, path_string, author_string)
    

def credentials(realm, username, may_save):
    """Return the default login credentials"""
    return True, svn_username, svn_password, False

def discover_changes(last_revision=pysvn.Revision(pysvn.opt_revision_kind.number, 0)):
    """Find out the changes occured since the last time this method is ran"""
    if last_revision is None:
        last_revision=pysvn.Revision(pysvn.opt_revision_kind.number, 0)
    client = pysvn.Client()
    client.callback_get_login = credentials
    log = client.log(
        svn_root, 
        discover_changed_paths=True,
        revision_end=last_revision
        )
    if len(log) is 1:
        return last_revision, None, None
    authors = []
    paths = []
    for entry in log[:-1]:
        if entry.author not in authors:
            authors.append(entry.author)
        for change in entry.changed_paths:
            path = change.path.split('/',2)[1]
            if path not in paths:
                paths.append(path)
    last_revision = log[0].revision
        
    return last_revision, authors, paths

if __name__ == '__main__':
    read_config()
    print 'Monitoring SVN repository: %s' % svn_root
    print '- Press %s to quit -' % '^C'
    last_revision = None
    while True:
        last_revision, authors, paths = discover_changes(last_revision)
        if paths is not None:
            log_message(paths, authors)
            notify(paths, authors)
        time.sleep(5 * 60)