#!/bin/bash
PID_FILE=".pids";
MYPATH="*.cfg"
FUNC=$1

if test "$1" == "" ; then
    echo "start/stop?"
fi

if [ "$FUNC" == "start" ]; then
    for f in $MYPATH
    do
        python svnmonitor.py $f &
    done
fi

if [ "$FUNC" == "stop" ]; then
    ps aux | grep "[p]ython svnmonitor" | awk '{print $2}' > tmp
    cat tmp | while read pid; do
        kill -9 $pid
    done
    rm tmp 2> /dev/null

    sleep 2
    ps aux | grep "[p]ython svnmonitor"

fi